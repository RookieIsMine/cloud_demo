package com.ssc.demo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "eureka-demo-provide")
public interface ConsumerService {
    @RequestMapping(value = "/demo1/hello",method = RequestMethod.GET)
    String demo1();
}
