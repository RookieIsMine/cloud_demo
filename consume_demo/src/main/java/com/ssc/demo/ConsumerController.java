package com.ssc.demo;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerController {
    @Autowired
    private RestTemplate restTemplate;

    //直接调用
    @GetMapping("/consumer/calldemo1")
    public String calldemo1() {
        return restTemplate.getForObject("http://localhost:8055/demo1/hello", String.class);
    }

    @GetMapping("/consumer/calldemo2")
    public String calldemo2() {
        return restTemplate.getForObject("http://eureka-demo-provide/demo1/hello", String.class);
    }

    @Autowired
    private ConsumerService consumerService;

    @GetMapping("/consumer/calldemo3")
    public String calldemo3() {
        String result = consumerService.demo1();
        return result;
    }

    @HystrixCommand(fallbackMethod = "reFailBack")
    @GetMapping("/consumer/calldemo4")
    public String calldemo4() {
        String result = consumerService.demo1();
        return result;
    }
   public String reFailBack(){
        return "快速失败了";
   }
}
