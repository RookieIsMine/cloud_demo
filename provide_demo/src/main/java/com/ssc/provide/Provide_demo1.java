package com.ssc.provide;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Provide_demo1 {
    @GetMapping("/demo1/hello")
     public String demo1(){
        return "test——demo1";
    }
}
