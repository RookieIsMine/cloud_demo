package com.ssc.zuul_demo;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.logging.Logger;

public class AccessFilter extends ZuulFilter {
    Logger logger=Logger.getLogger("AccessFilter");
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx= RequestContext.getCurrentContext();
        HttpServletRequest request=ctx.getRequest();
        System.out.println(request.getMethod()+"   "+request.getRequestURL().toString());
      //logger.info("send {}  request to {}",request.getMethod(),request.getRequestURL().toString());
        Object accessToken =request.getParameter("accessToken");
        if(accessToken==null){
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
        }
        return null;
    }
}
